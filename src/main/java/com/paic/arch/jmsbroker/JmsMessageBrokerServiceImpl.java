package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

/**
 * 接口实现
 * 
 * @author lvzicheng
 *
 */
public class JmsMessageBrokerServiceImpl implements JmsMessageBrokerService {
	
	private Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());

	private Connection connection;

	private Session session;

	private JmsMessageBroker broker;

	public JmsMessageBrokerServiceImpl(JmsMessageBroker broker) {
		super();
		this.broker = broker;
	}
	
	@Override
	public void startBroker() {
		
		try {
			
			broker.setBrokerService(new BrokerService());
			
			broker.getBrokerService().setPersistent(false);
			
			broker.getBrokerService().addConnector(broker.getBrokerUrl());
			
			broker.getBrokerService().start();
			
		} catch (Exception e) {
			
			logger.error("failed to start the broker!");
			
			throw new IllegalStateException(e);
			
		}
	}
	
	@Override
	public void stopBroker() {
		
		 if (broker.getBrokerService() == null) {
			 
	            throw new IllegalStateException("Cannot stop the broker from this API: " +
	            
	                    "perhaps it was started independently from this utility");
	            
	        }
		 
	        try {
	        	
				broker.getBrokerService().stop();
				
				 broker.getBrokerService().waitUntilStopped();
				 
			} catch (Exception e) {
				
				logger.error("failed to stop the broker!");

				throw new IllegalStateException(e);
			}
	       
	}

	@Override
	public void bindBrokerUrl(String brokerUrl) {

		broker.setBrokerUrl(brokerUrl);

	}

	@Override
	public String getBrokerUrl() {

		return broker.getBrokerUrl();

	}

	@Override
	public void sendTextMessage(String queueName, String message) {

		// 初始化连接
		initConnection();

		try {

			// 创建队列
			Destination queue = session.createQueue(queueName);

			// 创建消息生产者
			MessageProducer producer = session.createProducer(queue);

			// 发送消息
			producer.send(session.createTextMessage(message));

			// 关闭生产者
			producer.close();

		} catch (JMSException e) {
			
			logger.error("failed to send message!");

			throw new IllegalStateException(e);
			
		} finally {
			
			// 关闭连接
			closeConnection();
			
		}
	}

	@Override
	public String retrieveASingleMessage(String queueName, long timeOut) {
		// 初始化连接
		initConnection();
		
		String retrieveMessage = null;
		
		try {
			//创建队列
			Destination queue = session.createQueue(queueName);
			
			//创建消费者
			MessageConsumer consumer = session.createConsumer(queue);
			
			//接收消息
			Message message = consumer.receive(timeOut);
			
			//关闭消费者
			consumer.close();
			
			if (message == null) {
				
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", timeOut));
                
            }
			
			retrieveMessage = ((TextMessage) message).getText();
			
		} catch (JMSException e) {
			
			logger.error("failed to retrieve message!");

			throw new IllegalStateException(e);
			
		}finally{
			
			// 关闭连接
			closeConnection();
			
		}
		
		return retrieveMessage;
	}
	
	@Override
	public long getQueueMessageCount(String queueName) {
		
		long messageCount = 0;
		
		try {
			
			messageCount = getMessageCount(queueName);
			
		} catch (Exception e) {
			
			logger.error("failed to get message count, queue name is {}", queueName);

			throw new IllegalStateException(e);
			
		}
		return messageCount;
	}
	
	@Override
	public boolean isEmptyQueue(String queueName) {
		
		long messageCount = 0;
		
		try {
			
			 messageCount =  getMessageCount(queueName);
			 
		} catch (Exception e) {
			
			logger.error("failed to get message count, queue name is {}", queueName);

			throw new IllegalStateException(e);
			
		}
		
		return messageCount == 0;
	}

	/**
	 * 连接初始化
	 */
	private void initConnection() {

		try {

			connection = broker.getConnectionFactory().createConnection();

			connection.start();

		} catch (JMSException e) {

			logger.error("failed to init connection!");

			throw new IllegalStateException(e);

		}

		try {

			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		} catch (JMSException e) {

			logger.error("failed to init session!");

			throw new IllegalStateException(e);

		}
	}

	/**
	 * 连接关闭
	 */
	private void closeConnection() {

		try {

			// 关闭session
			if (session != null) {

				session.close();

			}

			// 关闭连接
			if (connection != null) {

				connection.close();

			}

		} catch (JMSException e) {

			logger.error("failed to close the connection!");

			throw new IllegalStateException(e);

		}
	}
	
	/**
	 * 根据队列名获取队列信息
	 * @param queueName
	 * @return
	 * @throws Exception
	 */
	 private DestinationStatistics getDestinationStatisticsFor(String queueName) throws Exception {
		 
	        Broker regionBroker = broker.getBrokerService().getRegionBroker();
	        
	        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
	        	
	            if (destination.getName().equals(queueName)) {
	            	
	                return destination.getDestinationStatistics();
	                
	            }
	        }
	        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", queueName, broker.getBrokerUrl()));
	    }
	 
	 /**
	  * 队列消息数量
	  * @param queueName
	  * @return
	  * @throws Exception
	  */
	 private long getMessageCount(String queueName) throws Exception{
		 
		long  messageCount = getDestinationStatisticsFor(queueName).getMessages().getCount();
		
		return messageCount;
	 }
	
	/**
	 * 接收消息为空异常类
	 * @author lvzicheng
	 *
	 */
	public class NoMessageReceivedException extends RuntimeException {
		
		private static final long serialVersionUID = 4190692219043891027L;
		

		public NoMessageReceivedException(String reason) {
			
            super(reason);
        }
    }


}
