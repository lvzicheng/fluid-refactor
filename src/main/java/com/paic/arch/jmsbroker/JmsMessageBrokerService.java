package com.paic.arch.jmsbroker;

/**
 * 接口类
 * @author lvzicheng
 *
 */
public interface JmsMessageBrokerService {
	
	/**
	 * 启动broker
	 */
	public void startBroker();
	
	/**
	 * 停止broker
	 */
	public void stopBroker();

	/**
	 * 绑定URL
	 * @param brokerUrl
	 */
	public void bindBrokerUrl(String brokerUrl);
	
	/**
	 * 获取URL
	 * @return
	 */
	public String getBrokerUrl();
	
	/**
	 * 发送消息
	 * @param queueName
	 * @param message
	 */
	public void sendTextMessage(String queueName, String message);
	
	/**
	 * 接收消息
	 * @param queueName
	 * @param timeOut
	 * @return
	 */
	public String retrieveASingleMessage(String queueName, long timeOut);
	
	/**
	 * 获取队列消息数量
	 * @param queueName
	 * @return
	 */
	public long getQueueMessageCount(String queueName);
	
	/**
	 * 队列是否为空
	 * @param queueName
	 * @return
	 */
	public boolean isEmptyQueue(String queueName);
	
}
